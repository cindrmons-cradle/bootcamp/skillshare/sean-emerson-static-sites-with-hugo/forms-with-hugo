"use strict";

// form elements
const contactForm = document.getElementById("js-contactForm");
const contactFormSubmitBtn = document.getElementById("js-contactFormSubmit");

// enable the button when JS is enabled
contactFormSubmitBtn.disabled = false;
contactFormSubmitBtn["aria-disabled"] = false;

// loading spinner
const spinner = document.createElement("div");
spinner.id = "js-loadingSpinner";
spinner.classList.add("spinner-border", "d-none");
contactForm.appendChild(spinner);

// alert message
const alert = document.createElement("div");
alert.id = "js-alertMessage";
alert.classList.add("alert", "d-none");
contactForm.appendChild(alert);

/* Helper Functions */
/* ------------------------------------------------ */
/**
 * message sender based on status; to be called by submitContactForm()
 *
 * @param {boolean} status Status of the XHR Response
 * @param {string} statusText Status Message
 */
function onMessage(status, statusText) {
  // add alert type based on status
  alert.classList.add(status === true ? "alert-success" : "alert-danger");

  // add message text
  alert.innerText = statusText;

  // unhide alert
  alert.classList.remove("d-none");
}

function resetForm() {
  // reset form contents
  contactForm.reset();

  // reset google recaptcha
  hcaptcha.reset();
}
/* ------------------------------------------------ */


/* Active Functions */
/* ------------------------------------------------ */

/**
 * Called by hCaptcha; handles submit form
 */
window.submitContactForm = () => {
  // show loading spinner
  spinner.classList.remove("d-none");

  // get form data
  const formData = new FormData(contactForm);

  // create new xhr instance
  const xhr = new XMLHttpRequest();

  // 1. open and configure xhr
  xhr.open(contactForm.method, contactForm.action);

  // 2. send data
  xhr.send(formData);

  // 3. set timeout
  xhr.timeout = 8000;

  // 4. handle timeout
  xhr.onTimeout = () => {
    onMessage(false, "Request Timed Out... Please try again later...");
  };

  // 5. on finish load
  xhr.onloadend = () => {
    // remove and reset validation
    contactForm.classList.remove("was-validated");

    // remove loading spinner
    spinner.classList.add("d-none");

    // get xhr repsonse
    const res = JSON.parse(xhr.response);

    // handle xhr response
    if (res.success) {
      onMessage(
        true,
        "Your message has been sent!\nWe'll get back to you shortly..."
      );
      resetForm();
      return;
    }
    if (res.error) {
      onMessage(false, `JSON Error!\n${res.error}`);
      return;
    }

    onMessage(false, `Something went wrong! Please try again later...`);
  };
};

// listen to submit event
contactForm.addEventListener(
  "submit",
  (event) => {
    // stop default submit behaviour
    event.preventDefault();
    event.stopPropagation();

    // add bootstrap classes for validation
    contactForm.classList.add("was-validated");

    // if current contactForm passed validation
    if (contactForm.checkValidity()) {
      // call hcaptcha for check and submission
      hcaptcha.execute();
      // hCaptcha calls onSubmit() after check
    }
  },
  false
);
/* ------------------------------------------------ */
