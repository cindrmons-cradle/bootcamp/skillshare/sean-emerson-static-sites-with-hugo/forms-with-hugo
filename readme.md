# Full length course - Creating a Blog with HUGO


This course takes you through the entire process of setting up a blog with HUGO - including creating the theme and all of the functions to display data.

To watch the entire course follow [this link to my skillshare course](https://skl.sh/3IKdWc2). You get 1 month free, to watch the course by using my referral link.

[See all of my full length courses here](https://www.skillshare.com/user/sean_emerson?teacherRef=684621925&via=teacher-referral-channel&gr_tch_ref=on&utm_campaign=teacher-referral-channel&utm_source=ShortUrl&utm_medium=teacher-referral-channel)


[View my YouTube channel here](https://www.youtube.com/channel/UCtlnMUJr68ytsr11_dv_elg)

## Screenshots of this Project

![Contact Form](./screenshots/screenshot-0.png)

![Form Invalid](./screenshots/screenshot-1.png)

![Form Valid](./screenshots/screenshot-2.png)

![Form hCaptcha](./screenshots/screenshot-3.png)

![Form Success](./screenshots/screenshot-4.png)

## Working with hCaptcha

For hCaptcha to work, please follow these steps:

1. Append the following to your [/etc/hosts](#hosts-folder-location) file:
    ```
    127.0.0.1         hugo.contactforms.com
    ```

2. While running `yarn dev`, you must go to [http://hugo.contactforms.com:1313/](http://hugo.contactforms.com:1313/) for hCaptcha to work.

### `hosts` folder location

| OS      | `hosts` folder location                 |
| ------- | --------------------------------------- |
| Windows | `C:\Windows\System32\Drivers\etc\hosts` |
| Mac     | `/private/etc/hosts`                    |
| Linux   | `/etc/hosts`                            |